sudo pacman -S xmonad xmonad-contrib xmobar kitty nitrogen picom dmenu ttf-ubuntu-font-family;
mkdir ~/.xmonad;
touch ~/.xmobarrc;
git clone https://github.com/Arielnoder/xmonad.git;
git clone https://github.com/Arielnoder/xmobar.git;
git clone https://gitlab.com/Arielnoder/kitty.git;
cd xmonad-script;
cp install.sh ~/;
cd;
cp $HOME/kitty/kitty.conf $HOME/.config/kitty/kitty.conf;
cp ~/xmobar/xmobarrc .xmobarrc;
cp ~/xmonad/xmonad.hs ~/.xmonad/ 


